<?php
require_once('inc/config.php')
?>
<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8" />
	    <title><?php echo $title; ?></title>
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <!-- Bootstrap -->
	    <link href="<?php echo WEBSITE; ?>style/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
		
	    <link href="<?php echo WEBSITE; ?>style/main.css" rel="stylesheet" media="screen">

		
	    <script src="<?php echo WEBSITE; ?>style/bootstrap/js/bootstrap.min.js"></script>
	    <link rel="stylesheet" type="text/css" href="<?php echo WEBSITE; ?>style/geshicss/dawn.css" />
	    
    </head>
    <body>
    	
    		<div class="row-fluid">
    			<div  id="container" class="span10 offset1">
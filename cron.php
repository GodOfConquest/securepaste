<?php
$actual = time();

$fichiers = scandir('./data/');
foreach($fichiers as &$file) // on parcourt tous les noms de fichiers voir si le fichier portant l'id indiqué existe
{
	preg_match("#^[\w]{10}-([\d]+)(\.txt)?$#i", $file, $matches);
	$name = $matches[0];
	$x = intval($matches[1]);
	
	if($x <= $actual && $x != 0 && $x !=1)
		unlink('./data/'.$name);
}